//
//  ImageLoader.m
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import "ImageLoader.h"
#import "Constants.h"
#import "ImageInfo.h"

@implementation ImageLoader

#pragma mark -

- (void)loadImagesInfoWithKeywords:(NSString *)keywords successBlock:(void (^)(NSArray *))successBlock {
    NSURL *url = [self urlWithKeywords:keywords];
    NSURLSessionDataTask *downloadImagesInfoTask = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSArray *images = [NSArray array];
        if (data) {
            images = [self processImagesResponseUsingData:data];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            successBlock(images);
        });
    }];
    [downloadImagesInfoTask resume];
}

- (NSURL *)urlWithKeywords:(NSString *)keywords {
    NSString *urlString = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&text=%@&per_page=20&format=json&nojsoncallback=1", kFlickrAPIKey, keywords];
    NSURL *url = [NSURL URLWithString:urlString];
    return url;
}

#pragma mark -

- (void)loadImagesWithUrl:(NSURL *)imageUrl successBlock:(void (^)(NSData *))successBlock {
    NSURLSessionDataTask *downloadImageTask = [[NSURLSession sharedSession] dataTaskWithURL:imageUrl completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        successBlock(data);
    }];
    [downloadImageTask resume];
}

#pragma mark - Data parsing

- (NSArray *)processImagesResponseUsingData:(NSData*)data {
    NSMutableArray *imagesInfoArray = [NSMutableArray new];
    NSError *parseJsonError = nil;
    
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingAllowFragments error:&parseJsonError];
    NSDictionary *photosDic = [jsonDict objectForKey:@"photos"];
    NSArray *photoArray = [photosDic objectForKey:@"photo"];
    for (NSDictionary *photoDic in photoArray) {
        ImageInfo *imageInfo = [[ImageInfo alloc] initWithDictionary:photoDic];
        [imagesInfoArray addObject:imageInfo];
    }
    return imagesInfoArray;
}


@end
