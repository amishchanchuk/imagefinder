//
//  ImageLoader.h
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageLoader : NSObject

- (void)loadImagesInfoWithKeywords:(NSString *)keywords successBlock:(void (^)(NSArray *images))successBlock;
- (void)loadImagesWithUrl:(NSURL *)imageUrl successBlock:(void (^)(NSData *imageData))successBlock;

@end
