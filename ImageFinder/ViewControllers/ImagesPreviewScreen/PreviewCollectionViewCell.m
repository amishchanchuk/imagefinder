//
//  PreviewCollectionViewCell.m
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import "PreviewCollectionViewCell.h"
#import "ImageLoader.h"

@interface PreviewCollectionViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *ivPreview;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation PreviewCollectionViewCell

- (void)setImageInfo:(id)imageInfo {
    _imageInfo = imageInfo;
    [self displayImageInfo];
}

- (void)displayImageInfo {
    self.lblTitle.text = self.imageInfo.imageTitle;
    self.ivPreview.image = nil;
    ImageLoader *imageLoader = [[ImageLoader alloc] init];
    if (self.imageInfo.smallImage) {
        self.ivPreview.image = self.imageInfo.smallImage;
        self.lblTitle.hidden = YES;
    }
    else {
        [self.activityIndicator startAnimating];
        [imageLoader loadImagesWithUrl:[NSURL URLWithString:self.imageInfo.smallImageUrlString] successBlock:^(NSData *imageData) {
            UIImage *downloadedImage = [UIImage imageWithData:imageData];
            self.imageInfo.smallImage = downloadedImage;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.ivPreview.image = downloadedImage;
                [self.activityIndicator stopAnimating];
                self.lblTitle.hidden = YES;
            });
        }];
    }
}

@end
