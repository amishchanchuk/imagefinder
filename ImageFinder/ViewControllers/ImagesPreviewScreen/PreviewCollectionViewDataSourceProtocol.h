//
//  PreviewCollectionViewDataSourceProtocol.h
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol PreviewCollectionViewDataSourceProtocol <NSObject, UICollectionViewDataSource>

@property (nonatomic, readonly) NSArray *largeImagesUrlArray;

@end
