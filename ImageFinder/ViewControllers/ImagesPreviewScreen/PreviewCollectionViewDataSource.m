//
//  PreviewCollectionViewDataSource.m
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import "PreviewCollectionViewDataSource.h"
#import "PreviewCollectionViewCell.h"

#define kBorderWidth 1.0f

@implementation PreviewCollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PreviewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PreviewCollectionViewCell" forIndexPath:indexPath];
    cell.layer.borderWidth = kBorderWidth;
    cell.layer.borderColor = [UIColor blackColor].CGColor;
    cell.imageInfo = [self.photosArray objectAtIndex:indexPath.row];
    return cell;
}

- (NSArray *)largeImagesUrlArray {
    NSMutableArray *urlArray = [NSMutableArray array];
    for (id<ImageInfoProtocol> imageInfo in self.photosArray) {
        NSURL *imageURL = [NSURL URLWithString:imageInfo.largeImageUrlString];
        [urlArray addObject:imageURL];
    }
    return urlArray;
}

@end
