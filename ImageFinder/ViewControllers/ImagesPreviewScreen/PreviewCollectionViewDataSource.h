//
//  PreviewCollectionViewDataSource.h
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PreviewCollectionViewDataSourceProtocol.h"

@interface PreviewCollectionViewDataSource : NSObject <PreviewCollectionViewDataSourceProtocol>

@property (nonatomic, strong) NSArray *photosArray;

@end
