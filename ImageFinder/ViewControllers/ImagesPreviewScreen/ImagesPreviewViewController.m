//
//  ViewController.m
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import "ImagesPreviewViewController.h"
#import "PreviewCollectionViewDataSource.h"
#import "ImageLoader.h"
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"

#define kNumberOfCellsInARowLandscape 2 //You can change count of column here
#define kLineSpacingCorrection 1

@interface ImagesPreviewViewController () <UICollectionViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *imagesCollectionView;
@property (nonatomic, strong) UISearchController *imagesSearchController;
@property (nonatomic, strong) PreviewCollectionViewDataSource *dataSource;
@property (nonatomic, strong) ImageLoader *imageLoader;

@end

@implementation ImagesPreviewViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.dataSource = [[PreviewCollectionViewDataSource alloc] init];
        self.imageLoader = [[ImageLoader alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSearchController];
    [self addSearchControllerToNavigationController];
}


#pragma mark - Setup

- (void)initSearchController {
    self.imagesSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.imagesSearchController.searchBar.delegate = self;
    self.imagesSearchController.hidesNavigationBarDuringPresentation = NO;
    self.imagesSearchController.dimsBackgroundDuringPresentation = YES;
}

- (void)addSearchControllerToNavigationController {
    self.navigationItem.titleView = self.imagesSearchController.searchBar;
    self.definesPresentationContext = YES;
}

#pragma mark -

- (void)showImagesWithKeywords:(NSString *)keywords {
    __weak ImagesPreviewViewController *weakSelf = self;
    [self.imageLoader loadImagesInfoWithKeywords:keywords successBlock:^(NSArray *images) {
        [weakSelf refreshCollectionViewDataSourceWithImages:images];
    }];
}

- (void)refreshCollectionViewDataSourceWithImages:(NSArray *)images {
    self.dataSource.photosArray = images;
    self.imagesCollectionView.dataSource = self.dataSource;
    [self.imagesCollectionView reloadData];
}


#pragma mark - UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewFlowLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
        CGFloat cellWidth = floor((self.imagesCollectionView.bounds.size.width - collectionViewLayout.minimumLineSpacing * kNumberOfCellsInARowLandscape - kLineSpacingCorrection) / kNumberOfCellsInARowLandscape);
        return CGSizeMake(cellWidth, collectionViewLayout.itemSize.height);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.imagesCollectionView reloadData];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self showLargeImagesWithInitialIndex:indexPath.row];
}

- (void)showLargeImagesWithInitialIndex:(NSInteger)initialIndex {
    NSArray *imagesUrl = [self.dataSource largeImagesUrlArray];
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:imagesUrl];
    [browser setInitialPageIndex:initialIndex];
    [self presentViewController:browser animated:YES completion:nil];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self showImagesWithKeywords:searchBar.text];
    self.imagesSearchController.active = NO;
}

@end
