//
//  PreviewCollectionViewCell.h
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageInfoProtocol.h"

@interface PreviewCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) id<ImageInfoProtocol> imageInfo;

@end
