//
//  ImageInfoProtocol.h
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ImageInfoProtocol <NSObject>

@property (nonatomic, readonly) NSString *imageTitle;
@property (nonatomic, readonly) NSString *smallImageUrlString;
@property (nonatomic, readonly) NSString *largeImageUrlString;
@property (nonatomic, strong) UIImage *smallImage; //Simple cache for small image for performance optimisation. Added only for small images, because large images cached in PhotoBrowser.

@end
