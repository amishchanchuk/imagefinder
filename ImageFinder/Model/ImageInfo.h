//
//  ImageInfo.h
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageInfoProtocol.h"

@interface ImageInfo : NSObject <ImageInfoProtocol>

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
