//
//  ImageInfo.m
//  ImageFinder
//
//  Created by Alexandr Mischanchuk on 10/11/16.
//  Copyright © 2016 Alexandr Mishchanchuk. All rights reserved.
//

#import "ImageInfo.h"

@interface ImageInfo ()

@property (nonatomic, strong) NSString *owner;
@property (nonatomic, strong) NSString *secret;
@property (nonatomic, strong) NSString *server;
@property (nonatomic, strong) NSString *photoId;
@property (nonatomic, strong) NSString *farm;
@property (nonatomic, strong) NSString *title;

@end

@implementation ImageInfo

@synthesize smallImage;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        _owner = [dictionary objectForKey:@"owner"];
        _secret = [dictionary objectForKey:@"secret"];
        _server = [dictionary objectForKey:@"server"];
        _photoId = [dictionary objectForKey:@"id"];
        _farm = [dictionary objectForKey:@"farm"];
        _title = [dictionary objectForKey:@"title"];
    }
    return self;
}

#pragma mark - ImageInfoProtocol

- (NSString *)imageTitle {
    return self.title;
}

- (NSString *)smallImageUrlString {
    return [self imageUrlStringWithSizeKey:@"m"];
}

- (NSString *)largeImageUrlString {
    return [self imageUrlStringWithSizeKey:@"b"];
}

#pragma mark -

- (NSString *)imageUrlStringWithSizeKey:(NSString *)sizeKey {
    return [NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@_%@.jpg", self.farm, self.server, self.photoId, self.secret, sizeKey];
}

@end
